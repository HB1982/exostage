<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="style.css" />
    <title>Document</title>
</head>

<body>
    <h1>Formulaire de contact</h1>

    <form action="checkform.php" method="post">
        <div >
            <label class="form-label"for="nom">Nom </label>
            <input class="form-control"type="text" id="nom" name="nom" style="width:30em">
        </div>
        <div class="mb-3">
            <label class="form-label"for="prenom">Prénom : </label>
            <input class="form-control" type="text" id="prenom" name="prenom" style="width:30em">
        </div>
        <div class="mb-3">
            <label class="form-label"for="mail">Email : </label>
            <input class="form-control"type="email" id="mail" name="mail">
        </div>
        <div class="mb-3">
            <label class="form-label"for="tel">Téléphone : </label>
            <input class="form-control" type="text" id="tel" placeholder='0600007788' name="tel" pattern="[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}" />
        </div>
        <div class="mb-3">
            <label class="form-label"for="age">Age : </label>
            <input class="form-control" type="number" id="age" name="age" min="2" max="99">
        </div>
        <div>
            <label class="form-label"for="lemessage">Votre message : </label>
            <textarea class="form-control"type="text" id="lemessage" name="lemessage"></textarea>
        </div>
        <div class="c100" id="submit">
            <input class="btn btn-primary" type="submit" value="Envoyer">
        </div>
    </form>

</body>

</html>