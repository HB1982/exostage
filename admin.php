<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire d'identification</title>
</head>


<body>
    <form class="column" id="centrer" action="login.php" method="post">
        <p>Votre login :</p> <input type="text" name="username" required>
        <p>Votre mot de passe :</p> <input type="password" name="psw" required>
        <input class="btncommande" type="submit" value="Connexion">
    </form>
    <a href="./logout.php">Déconnexion</a>
</body>

</html>