<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    

<h1>Formulaire de contact</h1>
<?php
   include 'data.php';
$resultat = getAllform();

   ?>

       <?php       
       
       foreach ($resultat as $i) {

       ?>

    <form action="checkmodif.php" method="post">
        <div >
            <label class="form-label"for="nom">Nom </label>
            <input value="<?php echo($i["nom"])?>" class="form-control" type="text" id="nom" name="nom" style="width:30em">
        </div>
        <div class="mb-3">
            <label class="form-label"for="prenom">Prénom : </label>
            <input value="<?php echo($i["prenom"])?>"class="form-control" type="text" id="prenom" name="prenom" style="width:30em">
        </div>
        <div class="mb-3">
            <label class="form-label"for="mail">Email : </label>
            <input value="<?php echo($i["mail"])?>"class="form-control"type="email" id="mail" name="mail">
        </div>
        <div class="mb-3">
            <label class="form-label"for="tel">Téléphone : </label>
            <input value="<?php echo($i["tel"])?>"class="form-control" type="text" id="tel" placeholder='0600007788' name="tel" pattern="[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}" />
        </div>
        <div class="mb-3">
            <label class="form-label"for="age">Age : </label>
            <input value="<?php echo($i["age"])?>"class="form-control" type="number" id="age" name="age" min="2" max="99">
        </div>
       
        <div class="c100" id="submit">
            <input type="hidden" name="id" value="<?php echo $i['id']; ?>">
            <input class="btn btn-primary" type="submit" value="Modifier">
        </div>
      <?php } ?>
    </form>
    </body>
</html>